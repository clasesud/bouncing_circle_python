import pygame
import sys

# Initialize Pygame
pygame.init()

# Set up the display
screen_width, screen_height = 640, 480
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Bouncing Circle')

# Circle details
circle_color = (255, 0, 0)  # Red
circle_radius = 20
circle_x, circle_y = screen_width // 2, screen_height // 2  # Start position
circle_speed_x, circle_speed_y = 2, 2  # Speed and direction

# Clock to control the frame rate
clock = pygame.time.Clock()

# Main game loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Update circle position
    circle_x += circle_speed_x
    circle_y += circle_speed_y

    # Check for bouncing
    if circle_x - circle_radius <= 0 or circle_x + circle_radius >= screen_width:
        circle_speed_x *= -1  # Reverse direction
    if circle_y - circle_radius <= 0 or circle_y + circle_radius >= screen_height:
        circle_speed_y *= -1

    # Fill the screen with black
    screen.fill((0, 0, 0))

    # Draw the circle
    pygame.draw.circle(screen, circle_color, (circle_x, circle_y), circle_radius)

    # Update the display
    pygame.display.flip()

    # Cap the frame rate
    clock.tick(60)

pygame.quit()
sys.exit()
