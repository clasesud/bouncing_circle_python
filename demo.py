from Persona import Persona

# Crear una instancia de la clase Persona
persona = Persona()

# Asignar valores a los atributos usando los setters

persona.nombre = "Juan"
persona.edad = 25
persona.identificacion = 123456789

#nacer
persona.nacer()

#crecer
persona.crecer()

#reproducir
persona.reproducir()

#morir
persona.morir()





