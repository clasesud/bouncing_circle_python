# Create the game instance
from Circle import Circle
from Game import Game


game = Game(640, 480, 'Bouncing Circle - Advanced OOP')

# Create a circle and add it to the game
circle = Circle(320, 240, 20, 2, 2, (255, 0, 0))
game.add_object(circle)

# Run the game
game.run()
