class Persona:
    
    # Cuando se antecede de _ el atributo es privado y se tienen que usar los setters y getters
    def __init__(self):
        self._nombre = None
        self._edad = None
        self._identificacion = None

    # @property es un decorador que permite leer los atributos privados
    #@property
    def nombre(self):
        return self._nombre

    # @nombre.setter es un decorador que permite modificar los atributos privados
    #@nombre.setter
    def nombre(self, value):
        self._nombre = value

    @property
    def edad(self):
        return self._edad

    @edad.setter
    def edad(self, value):
        self._edad = value

    @property
    def identificacion(self):
        return self._identificacion

    @identificacion.setter
    def identificacion(self, value):
        self._identificacion = value

    def nacer(self):
        print(f"{self.nombre} ha nacido.")

    def crecer(self):
        print(f"{self.nombre} está creciendo.")

    def reproducir(self):
        print(f"{self.nombre} puede reproducirse.")

    def morir(self):
        print(f"{self.nombre} ha muerto.")

# Crear una instancia de la clase Persona
persona = Persona()

# Asignar valores a los atributos usando los setters

persona.nombre = "Juan"
persona.edad = 25
persona.identificacion = 123456789

#nacer
persona.nacer()

#crecer
persona.crecer()

#reproducir
persona.reproducir()

#morir
persona.morir()
