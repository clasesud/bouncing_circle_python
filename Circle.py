import pygame

class Circle:
    def __init__(self, x, y, radius, speed_x, speed_y, color):
        self.x = x
        self.y = y
        self.radius = radius
        self.speed_x = speed_x
        self.speed_y = speed_y
        self.color = color

    def move(self, screen_width, screen_height):
        self.x += self.speed_x
        self.y += self.speed_y
        if self.x - self.radius <= 0 or self.x + self.radius >= screen_width or self.y - self.radius <= 0 or self.y + self.radius >= screen_height:
            self.bounce(screen_width, screen_height)

    def bounce(self, screen_width, screen_height):
        if self.x - self.radius <= 0 or self.x + self.radius >= screen_width:
            self.speed_x *= -1
        if self.y - self.radius <= 0 or self.y + self.radius >= screen_height:
            self.speed_y *= -1

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (self.x, self.y), self.radius)